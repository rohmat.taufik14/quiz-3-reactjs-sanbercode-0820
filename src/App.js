import React, { Component } from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import logo from '../public/img/logo.png';
import './App.css';
import Nav from './Route/Nav'
import Routes from './Route/Routes'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
        <header>
          <img id="logo" src={logo} alt="logo" width="200px" />
          <nav>
            <Nav />
          </nav>
        </header>

        <Routes />

        <footer>
          <h5>copyright &copy; 2020 by Sanbercode</h5>
        </footer>
        </Router>
      </div>
    );
  }
}

export default App;
