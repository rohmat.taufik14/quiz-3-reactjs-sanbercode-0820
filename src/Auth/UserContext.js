import React, {useState, useEffect, createContext} from 'react'
import axios from 'axios'

export const UserContext = createContext()

export const UserProvider = props => {
  const [users, setUsers] = useState([
    {username: "admin", password: "12345"},
    {username: "rohmat", password: "12345"},
  ])

  return (
    <UserContext.Provider value={[users, setUsers]}>
      {props.children}
    </UserContext.Provider>
  )
}
