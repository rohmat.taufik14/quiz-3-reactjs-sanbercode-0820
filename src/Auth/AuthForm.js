import React,{useContext, useState} from "react"
import {AuthContext, AuthProvider} from "./AuthContext"
import { Redirect, useHistory } from "react-router-dom";

const FormLogin = () => {
  const [auth, setAuth] = useContext(AuthContext)
  const [data, setData] = useState({username:"", password:""})
  const history = useHistory();

  const setUsername = (event) => {
    setData({...data, username: event.target.value})
  }

  const setPassword = (event) => {
    setData({...data, password: event.target.value})
  }

  const handleLogin = (event) => {
    if (data.username === "" || data.password === "") {
      setAuth(true,
        () => {
          history.push("/");
        })
    }
    console.log("masok")
  }

   if (auth === true) {
       return <Redirect to="/" />
   } else {
    return (<section>
      <h1>Form Login</h1>
      <div>
        Username <input type="text" onChange={setUsername} value={data.username}/>
        Password <input type="password" onChange={setPassword} value={data.password}/>
      </div>
      <button onClick={handleLogin}>Login</button>
    </section>
    )
    }

}

const AuthForm = () =>{
  return(
    <AuthProvider>
      <FormLogin/>
    </AuthProvider>
  )
}

export default AuthForm
