import React from 'react'

const dataPeserta = {
  nama: "Rohmat Taufik",
  email: "rohmat.taufik14@gmail.com",
  os: "MacOs",
  gitlab: "rohmat.taufik14",
  telegram: "@rohmattaufik"
}

const About = () => {
  return (
    <section>
      <div className="about-div">
        <h1>Data Peserta Sanbercode Bootcamp Reactjs</h1>
        <ol>
          <li><strong>Nama:</strong> {dataPeserta.nama}</li>
          <li><strong>Email:</strong> {dataPeserta.email}</li>
          <li><strong>Sistem Operasi yang digunakan:</strong> {dataPeserta.os}</li>
          <li><strong>Akun Gitlab:</strong> {dataPeserta.gitlab}</li>
          <li><strong>Akun Telegram:</strong> {dataPeserta.telegram}</li>
        </ol>
      </div>
    </section>
  )
}

export default About
