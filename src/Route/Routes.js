import React, {useContext} from "react";
import MovieList from '../Movie/MovieList'
import About from '../About/About'
import MovieEditor from '../Movie/MovieEditor'
import AuthForm from '../Auth/AuthForm'
import { Switch, Route } from "react-router";
import {AuthContext, AuthProvider} from "../Auth/AuthContext"
import PrivateRoute from './PrivateRoute'

const Routes = () => {
  const [auth] = useContext(AuthContext)

  return (
    <Switch>
      <Route exact path="/" component={MovieList} />
      <Route exact path="/about" component={About} />
      <Route exact path="/movie-list-editor" component={MovieEditor} />
      <Route exact path="/login" component={AuthForm} />
    </Switch>
  );
};

const RoutesList = () => {
  return(
    <AuthProvider>
      <Routes/>
    </AuthProvider>
  )
}

export default RoutesList;
