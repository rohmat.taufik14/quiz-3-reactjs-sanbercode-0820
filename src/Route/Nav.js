import React, {useContext} from "react";
import {Link} from "react-router-dom";
import {AuthContext, AuthProvider} from "../Auth/AuthContext"

const NavList = () => {
  const [auth] = useContext(AuthContext)

  return (
    <ul>
      <li>
        <Link to="/">Home</Link>
      </li>
      <li>
        <Link to="/about">About</Link>
      </li>
      <li className={!auth ? "" : "nav-hide"}>
        <Link to="/login">Login</Link>
      </li >
      <li>
        <Link to="/movie-list-editor">Movie List Editor</Link>
      </li>
    </ul>
  )
}

const Nav = () => {
  return(
    <AuthProvider>
      <NavList/>
    </AuthProvider>
  )
}

export default Nav
