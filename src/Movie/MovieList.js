import React, {useContext} from "react"
import {MovieContext} from "./MovieContext"
import {MovieProvider} from "./MovieContext"

const Movie = () => {
  const [movies] = useContext(MovieContext)

  return (
    <div id="movie-list">
			{movies.map((el, i)=> {
				return (
          <div className="movie" key={i}>
            <h3>{el.title}</h3>
            <div className="thumbnail">
              <div className="image">
                <img src={el.image_url}  alt={el.title} />
              </div>
              <div className="info">
                <h4>Rating: {el.rating}</h4>
                <h4>Durasi: {el.duration}</h4>
                <h4>Genre: {el.genre}</h4>
              </div>
            </div>
            <p>
              {el.description}
            </p>
          </div>
				)
			})}
    </div>
  );
}

const MovieList = () =>{

  return(
    <section>
      <h1>Daftar Film Terbaik</h1>
      <MovieProvider>
        <Movie />
      </MovieProvider>
    </section>
  )
}

export default MovieList
