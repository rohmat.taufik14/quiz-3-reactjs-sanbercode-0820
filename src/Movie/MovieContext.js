import React, {useState, useEffect, createContext} from 'react'
import axios from 'axios'

export const MovieContext = createContext()

export const MovieProvider = props => {
  const [movies, setMovies] = useState([])

  useEffect( () => {
		getData()
  }, [])

	const getData = () => {
    axios.get(`http://backendexample.sanbercloud.com/api/movies`)
    .then(res => {
      setMovies(res.data)
    })
	}

  return (
    <MovieContext.Provider value={[movies, setMovies]}>
      {props.children}
    </MovieContext.Provider>
  )
}
