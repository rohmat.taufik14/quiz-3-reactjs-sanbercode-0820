import React, {useContext} from "react"
import {MovieContext} from "./MovieContext"
import {MovieUpdateContext} from "./MovieUpdateContext"
import axios from "axios"

const MovieEditorForm = () => {
  const [movies, setMovies] = useContext(MovieContext)
  const [movie, setMovie] = useContext(MovieUpdateContext)

  const clear = () => {
    setMovie({
      id: null,
      title: "",
      description: "",
      year: null,
      duration: 0,
      genre: "",
      rating: 0,
      review: null,
      image_url: ""
    })
  }

  const handleSubmit = (event) =>{
    event.preventDefault()
    if (movie.year < 1980){
      alert("Year invalid! year harus lebih besar dari 1979")
    } else if (movie.rating < 0 || movie.year > 10) {
      alert("Rating invalid! rating harus diantara 0 sampai 10")
    } else {
      if (movie.id === null){
        handleAdd()
      } else {
        handleUpdate()
      }
    }
  }

  const handleAdd = () => {
    axios.post(`http://backendexample.sanbercloud.com/api/movies`,
      {...movie})
	    .then(res => {
	      getData()
        clear()
	    })
  }

  const handleUpdate = () => {
    axios.put(`http://backendexample.sanbercloud.com/api/movies/` + movie.id,
			{...movie})
	    .then(res => {
	      getData()
        clear()
	    })
  }

  const getData = () => {
    axios.get(`http://backendexample.sanbercloud.com/api/movies`)
    .then(res => {
      setMovies(res.data)
    })
	}

  const handleChangeTitle = (event) =>{
    setMovie({...movie, title: event.target.value})
  }

  const handleChangeYear = (event) =>{
    setMovie({...movie, year: event.target.value})
  }

  const handleChangeDuration = (event) =>{
    setMovie({...movie, duration: event.target.value})
  }

  const handleChangeGenre = (event) =>{
    setMovie({...movie, genre: event.target.value})
  }

  const handleChangeRating = (event) =>{
    setMovie({...movie, rating: event.target.value})
  }

  const handleChangeDescription = (event) =>{
    setMovie({...movie, description: event.target.value})
  }

  const handleChangeImageUrl = (event) =>{
    setMovie({...movie, image_url: event.target.value})
  }

  return(
    <section>
      <h1>Movie Form</h1>
      <div className="movie-editor-form">
        <form>
          <div className="form-group">
            <div className="label">Title</div>
            <div className="input">
              <input type="text" value={movie.title} onChange={handleChangeTitle} />
            </div>
          </div>
          <div className="form-group">
            <div className="label">Description</div>
            <div className="input">
              <textarea value={movie.description} onChange={handleChangeDescription}>
              </textarea>
            </div>
          </div>
          <div className="form-group">
            <div className="label">Year</div>
            <div className="input">
              <input type="number" value={movie.year} onChange={handleChangeYear} />
            </div>
          </div>
          <div className="form-group">
            <div className="label">Duration</div>
            <div className="input">
              <input type="number" value={movie.duration} onChange={handleChangeDuration} />
            </div>
          </div>
          <div className="form-group">
            <div className="label">Genre</div>
            <div className="input">
              <input type="text" value={movie.genre} onChange={handleChangeGenre} />
            </div>
          </div>
          <div className="form-group">
            <div className="label">Rating</div>
            <div className="input">
              <input type="number" value={movie.rating} onChange={handleChangeRating} />
            </div>
          </div>
          <div className="form-group">
            <div className="label">Image Url</div>
            <div className="input">
              <textarea value={movie.image_url} onChange={handleChangeImageUrl}>
              </textarea>
            </div>
          </div>
          <button className="button-success" onClick={handleSubmit}>Submit</button>
          <button className="button-warning" onClick={clear}>Clear</button>
        </form>
      </div>
    </section>
  )

}

export default MovieEditorForm
