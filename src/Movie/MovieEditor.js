import React from "react"
import {MovieProvider} from "./MovieContext"
import {MovieUpdateProvider} from "./MovieUpdateContext"
import MovieEditorList from "./MovieEditorList"
import MovieEditorForm from "./MovieEditorForm"

const Movie = () =>{
  return(
    <MovieProvider>
      <MovieUpdateProvider>
        <MovieEditorList/>
        <MovieEditorForm/>
      </MovieUpdateProvider>
    </MovieProvider>
  )
}

export default Movie
