import React, {useContext, useState} from "react"
import {MovieContext} from "./MovieContext"
import axios from "axios"

const MovieSearch = () =>{
  const [movies, setMovies] = useContext(MovieContext)
  const [search, setSearch] = useState("");

  const setChangeSearch = (event) => {
    setSearch(event.target.value)
  }

  const handleSearch = () => {
    let movies = [];
    axios.get(`http://backendexample.sanbercloud.com/api/movies`)
      .then(res => {
        if (search.length > 0){
          movies = (res.data).filter(function(movie){
            return (movie.title).includes(search);
          });
        } else {
          movies = res.data
        }
      }).finally(() => {
        setMovies(movies)
      })
	}

  return(
    <div>
      <input type="text" onChange={setChangeSearch} value={search} />
      <button onClick={handleSearch}>Search</button>
    </div>
  )
}

export default MovieSearch
