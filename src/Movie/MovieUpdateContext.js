import React, {useState, createContext} from 'react'

export const MovieUpdateContext = createContext()

export const MovieUpdateProvider = props => {
  const [movie, setMovie] = useState({
    id: null,
    title: "",
    description: "",
    year: 2020,
    duration: 120,
    genre: "",
    rating: 0,
    review: null,
    image_url: ""
  })

  return (
    <MovieUpdateContext.Provider value={[movie, setMovie]}>
      {props.children}
    </MovieUpdateContext.Provider>
  )
}
