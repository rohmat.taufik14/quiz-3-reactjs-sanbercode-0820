import React, {useContext} from "react"
import {MovieContext} from "./MovieContext"
import {MovieUpdateContext} from "./MovieUpdateContext"
import MovieSearch from "./MovieSearch"
import axios from "axios"

const MovieEditorList = () =>{
  const [movies, setMovies] = useContext(MovieContext)
  const [movie, setMovie] = useContext(MovieUpdateContext)

  const handleDelete = (event) => {
    let index = event.target.value
		axios.delete(`http://backendexample.sanbercloud.com/api/movies/` + index)
	    .then(res => {
	      getData()
	    })
  }

  const handleUpdate = (event) => {
    setMovie(movies[event.target.value])
  }

  const getData = () => {
    axios.get(`http://backendexample.sanbercloud.com/api/movies`)
      .then(res => {
        setMovies(res.data)
      })
	}

  return(
    <section>
      <h1>Daftar Film</h1>
      <MovieSearch />
      <table id="table">
        <thead>
          <tr>
            <th>No</th>
            <th>Title</th>
            <th>Description</th>
            <th>Year</th>
            <th>Duration</th>
            <th>Genre</th>
            <th>Rating</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {movies.map((el, i) => {
            return (
              <tr className={"row-movie"} key={i}>
                <td>{i+1}</td>
                <td>{el.title}</td>
                <td>{ el.description !== null ? (el.description).substr(0,20).concat('...') : ""}</td>
                <td>{el.year}</td>
                <td>{el.duration}</td>
                <td>{el.genre}</td>
                <td>{el.rating}</td>
                <td>
                  <button className="button-info" onClick={handleUpdate} value={i}>Update</button>
                  <button className="button-danger" onClick={handleDelete} value={el.id}>Delete</button>
                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </section>
  )
}

export default MovieEditorList
